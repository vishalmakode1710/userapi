﻿using UserAPI.Model;

namespace UserAPI.Service
{
    public interface IuserService
    {
        List<User> GetAlluser();
        bool RegisterUser(User user);
        bool DeletUser(int id);
        User GetUserById(int id);
        bool EditUser(int id, User user);
        bool BlockUnBlockUser(int id, bool blockUnblockUser);
        User LogIn(LoginUser loginUser);
    }
}
