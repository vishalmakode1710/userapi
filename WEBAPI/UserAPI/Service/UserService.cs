﻿using UserAPI.Model;
using UserAPI.Repository;

namespace UserAPI.Service
{
    public class UserService : IuserService
    {
        readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }


        public bool BlockUnBlockUser(int id, bool blockUnblockUser)
        {
            User userExit = _userRepository.GetUserById(id);

            if (userExit == null)
            {
                return false;
            }
            else
            {
                int blockStatus = _userRepository.BlockUnblockuser(blockUnblockUser, userExit);
                if (blockStatus == 1)
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }
        
        }
        public bool DeletUser(int id)
        {
            User userExist = _userRepository.GetUserById(id);
            if (userExist != null)
            {
                int userDeleteStatus = _userRepository.DeleteUser(userExist);
                if(userDeleteStatus==1)
                {
                    return true;

                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool EditUser(int id, User user)
        {
            User userExist = _userRepository.GetUserById(id);
            if (userExist == null)
            {
                return false;
            }
            else
            {
                int userEditStatus = _userRepository.EditUser(userExist);
                if(userEditStatus==1)
                {
                    return true ;
                }
                else
                {
                    return false;
                }
            }
        }

        public List<User> GetAlluser()
        {
            return _userRepository.GetAllUsers();
        }

        public User GetUserById(int id)
        {
            return _userRepository.GetUserById(id);
        }

        public User LogIn(LoginUser loginUser)
        {
            return _userRepository.LogIn(loginUser.Name, loginUser.Password);
        }

        public bool RegisterUser(User user)
        {
            var userExist = _userRepository.GetUserByName(user.Name);
            if (userExist == null)
            {
                int RegisterUserStatus = _userRepository.RegisterUser(user);
                if(RegisterUserStatus==1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else return false;
        }
    }
}
