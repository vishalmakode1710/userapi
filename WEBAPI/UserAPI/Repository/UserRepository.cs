﻿using Microsoft.EntityFrameworkCore;
using UserAPI.Context;
using UserAPI.Model;

namespace UserAPI.Repository
{
    public class UserRepository : IUserRepository
    {
        readonly UserDbContext _userDbContext;
        public UserRepository(UserDbContext userDbContext)
        {
            _userDbContext = userDbContext;
        }
        public int BlockUnblockuser(bool blockUnblockUser, User userExit)
        {
            userExit.IsBlocked = blockUnblockUser;
            _userDbContext.Entry(userExit).State = EntityState.Modified;
            return _userDbContext.SaveChanges ();
        }

        public int DeleteUser(User userExist)
        {
            _userDbContext.userList.Remove(userExist);
            return _userDbContext.SaveChanges();
        }

        public int EditUser(User userExist)
        {
            _userDbContext.Entry(userExist).State = EntityState.Modified;
            return _userDbContext.SaveChanges();
        }

        public List<User> GetAllUsers()
        {
            return _userDbContext.userList.ToList();
        }

        public User GetUserById(int id)
        {
            return _userDbContext.userList.Where(u => u.Id == id).FirstOrDefault();
        }

        public User GetUserByName(string name)
        {
            return _userDbContext.userList.Where(u => u.Name == name).FirstOrDefault();
        }

        public User LogIn(string name, string password)
        {
            return _userDbContext.userList.Where(u=>u.Name == name && u.Password ==  password).FirstOrDefault();
        }

        public int RegisterUser(User user)
        {
            _userDbContext.userList.Add(user);
            return _userDbContext.SaveChanges();
        }
    }
}
